package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    private List<FridgeItem> foods = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return foods.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(nItemsInFridge() < 20) {
            foods.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!foods.contains(item)){
            throw new NoSuchElementException("This item is not to find in this fridge");
        }
        foods.remove(item);
    }

    @Override
    public void emptyFridge() {
        foods.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
        for(FridgeItem f : foods){
            if (f.hasExpired()){
                expiredFood.add(f);
            }
        }
        for(FridgeItem f : expiredFood){
            foods.remove(f);
        }
        return expiredFood;
    }
    
}
